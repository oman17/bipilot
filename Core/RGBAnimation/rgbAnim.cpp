/*
 * rgbAnim.cpp
 *
 *  Created on: 5 lut 2022
 *      Author: MZalas
 */

#include "rgbAnim.hpp"

cRGBAnim RGBAnim;

cRGBAnim::cRGBAnim()
{
	//this->Begin();
}

void cRGBAnim::Begin()
{
	this->InitVar();
	this->InitChooseRand();
}

void cRGBAnim::InitVar()
{
	for(uint8_t i=0; i < _FUNC_NUMBER; i++)
	{
		FuncBuffIdx[i] = i ;
	}
	this->FunTaken = 0;
}

void cRGBAnim::InitChooseRand()
{
	extern RNG_HandleTypeDef hrng;
	uint32_t seed;
	HAL_RNG_GenerateRandomNumber(&hrng, &seed);
	uint8_t number = seed % _FUNC_NUMBER;
	uint8_t i = 0;
	if( this->_FUNC_NUMBER - this->FunTaken < this->_FUNC_CHOOSE)
	{
		this->InitVar();
	}
	while(i < _FUNC_CHOOSE)
	{
		while(this->FuncBuffIdx[number] == 0)
		{
			HAL_RNG_GenerateRandomNumber(&hrng, &seed);
			number = seed % _FUNC_NUMBER;
		}
		this->ChoosedFunc[i] = number;
		this->FuncBuffIdx[number] = 0;
		this->FunTaken++;
		i++;
	}

	if( this->_FUNC_NUMBER - this->FunTaken < this->_FUNC_CHOOSE)
	{
		this->InitVar();
	}
}

/**
 * \brief           Circle RGB animation
 */
void cRGBAnim::LoadMany(uint16_t delay)
{
	for (uint8_t i = 0; i < this->NumOfPixels(); i++) {
		this->SetPixelColor((i + 0) % this->NumOfPixels(), 0x1F, 0, 0);
		this->SetPixelColor((i + 1) % this->NumOfPixels(), 0x1F, 0, 0);
		this->SetPixelColor((i + 2) % this->NumOfPixels(), 0, 0x1F, 0);
		this->SetPixelColor((i + 3) % this->NumOfPixels(), 0, 0x1F, 0);
		this->SetPixelColor((i + 4) % this->NumOfPixels(), 0, 0, 0x1F);
		this->SetPixelColor((i + 5) % this->NumOfPixels(), 0, 0, 0x1F);
		this->SetPixelColor((i + 6) % this->NumOfPixels(), 0x1F,0x1F,0x1F);
		this->SetPixelColor((i + 7) % this->NumOfPixels(), 0x1F,0x1F,0x1F);
		this->Show();
		this->SetAll(0, 0, 0);
	}
	this->Show();
	osDelay(pdMS_TO_TICKS(delay));
}

/**
 * \brief           Circle RGB animation, change of diode colours every two tic , add delay after call.
 *\param[in]       RGB first color, RGB second color
 */
void cRGBAnim::ToggleColor(cWS2812x::sRGB color1 , cWS2812x::sRGB color2, uint16_t delay)
{
	static uint8_t order;
	order++;
	for (uint8_t i = 0; i < this->NumOfPixels(); i++)
	{
		if((order % 2 == 0 && i%2 == 0 ) || (order % 2 == 1 && i%2 == 1 ))
		{
			this->SetPixelColor(i , color1.r, color1.g, color1.b);
		}else{
			this->SetPixelColor(i , color2.r, color2.g, color2.b);
		}
	}

	this->Show();
	this->SetAll(0, 0, 0);
	osDelay(pdMS_TO_TICKS(delay));
}

/**
 * \brief             Fill circle with color one by one, forward direction.
 */
uint8_t cRGBAnim::Fill(cWS2812x::sRGB color, uint16_t delay)
{


	static uint8_t cnt_led;
	this->SetPixelColor(cnt_led % this->NumOfPixels(), color);
	this->Show();
	cnt_led++;
	osDelay(pdMS_TO_TICKS(delay));

	if(cnt_led % this->NumOfPixels() == 0)
	{
		cnt_led=0;
		return 1;
	}
	return 0;
}

/**
 * \brief       Fill all diodes with random color.
 */
void cRGBAnim::FillRand(uint16_t delay) //todo: usunać delay jako argument ?
{
	cWS2812x::sRGB color;
	extern RNG_HandleTypeDef hrng;
	color.r= HAL_RNG_GetRandomNumber(&hrng);
	color.g= HAL_RNG_GetRandomNumber(&hrng);
	color.b= HAL_RNG_GetRandomNumber(&hrng);
	this->Fill(color ,delay);
}

/**
 * \brief           Fill strip with one color, go back with another and again froward third color to end.
 */
uint8_t cRGBAnim::Fill(cWS2812x::sRGB color1 , cWS2812x::sRGB color2, cWS2812x::sRGB color3 , uint16_t delay)
{
	static uint8_t color;
	if(color==0)
	{
		if(this->Fill(color1 ,delay))
			color++;
	}else if(color==1)
	{
		if(this->FillReverse(color2,delay))
			color++;
	}else{
		if(this->Fill(color3 ,delay))
		{
			color=0;
			this->Show();
			osDelay(pdMS_TO_TICKS(delay));
			return 1;
		}
	}
	this->Show();
	osDelay(pdMS_TO_TICKS(delay));
	return 0;
}

/**
 * \brief           Fill strip with one color, go back with another and again froward third color to end etc.
 */
uint8_t cRGBAnim::Fill(cWS2812x::sRGB color1 , cWS2812x::sRGB color2, cWS2812x::sRGB color3, cWS2812x::sRGB color4, uint16_t delay)
{
	static uint8_t color;
	if(color==0)
	{
		if(this->Fill(color1 ,delay))
			color++;
	}else if(color==1)
	{
		if(this->FillReverse(color2 ,delay))
			color++;
	}else if(color==2)
	{
		if(this->Fill(color3 ,delay))
			color++;
	}else{
		if(this->FillReverse(color4 ,delay))
		{
			color=0;
			this->Show();
			osDelay(pdMS_TO_TICKS(delay));
			return 1;
		}
	}
	this->Show();
	osDelay(pdMS_TO_TICKS(delay));
	return 0;
}

/**
 * \brief      Fill circle with color all the same time, forward direction, but smooth.
 */
uint8_t cRGBAnim::FillSmoothAll(uint32_t hue, uint16_t delay)
{

	static uint8_t Bright=0;
	static uint32_t hue2=0;
	hue2=this->ColorHSV(hue, 255, Bright);
	Bright++;
	this->SetAll(hue2);
	this->Show();
	osDelay(pdMS_TO_TICKS(delay));
	if(Bright>=255)
	{
		Bright=0;
		return 1;
	}
	return 0;
}

/**
 * \brief      Fill circle with color one by one, forward direction, but smooth.
 */
uint8_t cRGBAnim::FillSmooth(cWS2812x::sRGB color, uint16_t delay)
{
	static uint8_t cnt_led;
	static cWS2812x::sRGB iter;

	uint8_t check=0;
	if(iter.r<color.r)
	{
		iter.r++;
		check++;
	}
	if(iter.g<color.g)
	{
		iter.g++;
		check++;
	}
	if(iter.b<color.b)
	{
		iter.b++;
		check++;
	}
	this->SetPixelColor(cnt_led % this->NumOfPixels(), this->CalcPWM(iter));
	this->Show();

	if(check==0)
	{
		cnt_led++;
		iter.r=0;
		iter.g=0;
		iter.b=0;
		if(cnt_led % this->NumOfPixels() == 0)
		{
			cnt_led=0;
			osDelay(pdMS_TO_TICKS(delay));
			return 1;
		}
	}
	osDelay(pdMS_TO_TICKS(delay));
	return 0;
}

/**
 * \brief          Fill circle with color one by one, reverse direction.
 */
uint8_t cRGBAnim::FillReverse(cWS2812x::sRGB color, uint16_t delay)
{
	static uint8_t cnt_led=this->NumOfPixels();
	this->SetPixelColor(cnt_led-1, color.r, color.g, color.b);
	this->Show();
	cnt_led--;
	if(cnt_led == 0)
	{
		cnt_led=this->NumOfPixels();
		osDelay(pdMS_TO_TICKS(delay));
		return 1;
	}
	osDelay(pdMS_TO_TICKS(delay));
	return 0;
}

/**
 * \brief          Fill dark one by one reverse.
 */
uint8_t cRGBAnim::FillReverseDark(uint16_t delay)
{
	static uint8_t cnt_led=this->NumOfPixels()+1;
	this->SetPixelColor(cnt_led-1, 0, 0, 0);
	this->Show();
	cnt_led--;
	if(cnt_led == 0)
	{
		cnt_led=this->NumOfPixels()+1;
		osDelay(pdMS_TO_TICKS(delay));
		return 1;
	}
	osDelay(pdMS_TO_TICKS(delay));
	return 0;
}

/**
 * \brief          Fill diode symmetry one by one ,and reverse: 1-0-0-1,0-1-1-0; 1-0-0-1
 */
uint8_t cRGBAnim::FillSymetry(uint16_t delay)
{
	static uint8_t cnt_led=this->NumOfPixels()+1;

	static uint16_t pixelHue=0;
	pixelHue+=(65536/((this->NumOfPixels()/2)*2)); //max/(LED*lopp)
	this->SetPixelColor(cnt_led-1, this->ColorHSV(pixelHue));
	this->SetPixelColor(this->NumOfPixels()-cnt_led, this->ColorHSV(pixelHue));
	this->Show();
	cnt_led--;
	if(cnt_led == (this->NumOfPixels()+1)/2)
	{
		cnt_led=this->NumOfPixels()+1;
		osDelay(pdMS_TO_TICKS(delay));
		return 1;
	}
	osDelay(pdMS_TO_TICKS(delay));
	return 0;
}

/**
 * \brief          Fill diode one by one symmetry and reverse; 1-0-0-0-1,0-1-0-1-0
 */
uint8_t cRGBAnim::FillSymetryOne(uint16_t delay)
{
	static int8_t cnt_led=(this->NumOfPixels())/2-1;

	static uint16_t pixelHue=0;

	this->SetPixelColor(cnt_led, this->ColorHSV(pixelHue));
	this->SetPixelColor(this->NumOfPixels()-cnt_led-1, this->ColorHSV(pixelHue));
	this->Show();
	this->Clear();
	cnt_led--;
	if(cnt_led < 0)	//all led
	{
		pixelHue+=(65536/((this->NumOfPixels()/2)*2)); //max/(LED*lopp)
		cnt_led=(this->NumOfPixels())-1;
		osDelay(pdMS_TO_TICKS(delay));
		return 1;
	}else if(cnt_led == (this->NumOfPixels())/2) //if in half way
	{
		pixelHue+=(65536/((this->NumOfPixels()/2)*2)); //max/(LED*lopp)
	}
	osDelay(pdMS_TO_TICKS(delay));
	return 0;
}

/**
 * \brief          Fill diode one by one reverse symmetry 0-1-1-0,1-0-0-1
 */
uint8_t cRGBAnim::FillSymetryOneRev(uint16_t delay)
{
	static int8_t cnt_led=(this->NumOfPixels())/2-1;

	static uint16_t pixelHue=0;

	this->SetPixelColor(cnt_led, this->ColorHSV(pixelHue));
	this->SetPixelColor(this->NumOfPixels()-cnt_led-1, this->ColorHSV(pixelHue));
	this->Show();
	this->Clear();
	cnt_led--;
	if(cnt_led < 0)
	{
		pixelHue+=(65536/((this->NumOfPixels()/2)*2)); //max/(LED*lopp)
		cnt_led=(this->NumOfPixels())/2-1;
		osDelay(pdMS_TO_TICKS(delay));
		return 1;
	}
	osDelay(pdMS_TO_TICKS(delay));
	return 0;
}



/**
 * \brief/
 */
uint8_t cRGBAnim::ToggleShine(cWS2812x::sRGB color1 , cWS2812x::sRGB color2, cWS2812x::sRGB color3, uint16_t delay)
{
	static uint8_t color;
	if(color==0)
	{
		static uint8_t i=this->ShineDiv;
		i--;
		this->SetAll(0x00,0x00,0x00);
		if(i <= 1)
		{
			color++;
			i=this->ShineDiv;
		}


	}else if(color==1)
	{
		static uint8_t i=this->ShineDiv;
		i--;
		for(uint8_t j=0; j< this->NumOfPixels(); j++)
		{
			if(j % 3 ==0)
			{
				this->SetPixelColor(j , color1.r/i, color1.g/i ,color1.b/i);
			}
		}
		if(i <= 1)
		{
			color++;
			i=this->ShineDiv;
		}


	}else if(color==2)
	{
		static uint8_t i=this->ShineDiv;
		i--;
		for(uint8_t j=0; j< this->NumOfPixels(); j++)
		{
			if(j % 3 ==1)
			{
				this->SetPixelColor(j , color2.r/i, color2.g/i ,color2.b/i);
			}
		}
		if(i <= 1)
		{
			color++;
			i=this->ShineDiv;
		}

	}else{
		static uint8_t i=this->ShineDiv;
		i--;
		for(uint8_t j=0; j< this->NumOfPixels(); j++)
		{
			if(j % 3 ==2)
			{
				this->SetPixelColor(j , color3.r/i, color3.g/i ,color3.b/i);
			}
		}
		if(i <= 1)
		{
			color=0;
			i=this->ShineDiv;
			osDelay(pdMS_TO_TICKS(delay));
			return 1;
		}

	}
	this->Show();
	osDelay(pdMS_TO_TICKS(delay));
	return 0;
}


uint8_t cRGBAnim::DarkAllSmoothHSV(uint16_t hue ,uint16_t delay)
{
	static uint8_t Bright=255;
	static uint32_t hue2=0;
	hue2=this->ColorHSV(hue, 255, Bright);
	Bright--;
	this->SetAll(hue2);
	this->Show();
	osDelay(pdMS_TO_TICKS(delay));
	if(Bright<=1)
	{
		Bright=255;
		return 1;
	}
	return 0;
}

uint8_t cRGBAnim::DarkAllSmooth(uint16_t delay)
{
	uint8_t diode_dark=0;

    for (size_t index = 0; index < this->NumOfPixels(); index++) {
    	cWS2812x::cWS2812x::sRGB RGBTemp;

    	RGBTemp.r=this->GetPixel(index, 0);
    	RGBTemp.g=this->GetPixel(index, 1);
    	RGBTemp.b=this->GetPixel(index, 2);

    	if( RGBTemp.r > 0)
    		RGBTemp.r -=1;
    	else
    		diode_dark++;

    	if( RGBTemp.g >0)
    		RGBTemp.g -=1;
    	else
    		diode_dark++;
    	if( RGBTemp.b >0)
    		RGBTemp.b -=1;
    	else
    		diode_dark++;

    	this->SetPixelColorNoBright(index, RGBTemp);
    }
	if(diode_dark==this->NumOfPixels()*3)
	{
		return 1;
	}
	this->Show();
	osDelay(pdMS_TO_TICKS(delay));
	return 0;
}

// Theater-marquee-style chasing lights. Pass in a color (32-bit value,
// a la strip.Color(r,g,b) as mentioned above), and a delay time (in ms)
// between frames.
void cRGBAnim::TheaterChase(uint32_t color, uint16_t wait) {
  for(int a=0; a<10; a++) {  // Repeat 10 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      this->Clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in steps of 3...
      for(int c=b; c<this->NumOfPixels(); c += 3) {
        this->SetPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      this->Show(); // Update strip with new contents
      osDelay(pdMS_TO_TICKS(wait));  // Pause for a moment
    }
  }
}

/**
 * \brief     Change colors in all diode, shine and dark, rainbow
 */
void cRGBAnim::RainbowSeq(uint16_t delay)
{
	//cWS2812x::sRGB Colors[]={_RGB_RED, _RGB_ORANGE, _RGB_YELLOW, _RGB_GREEN, _RGB_BLUE, _RGB_PURPLE};
	uint16_t colorsHSV[]={0,20,56,110,240,290};
	//uint16_t colorsHSV[]={0,360/6*2,360/6*3,360/6*4,360/6*5,360};

	delay/=15;
	for(uint8_t i=0;i<sizeof(colorsHSV)/sizeof(colorsHSV[0]);i++)
	{
			uint16_t hsv= Przelicz_zakres(colorsHSV[i], 0, 0, 360, 65535);
			while(!this->FillSmoothAll(hsv, delay));
			while(!this->DarkAllSmoothHSV(hsv,delay));
	}
}

// Rainbow cycle along whole this-> Pass delay time (in ms) between frames.
void cRGBAnim::Rainbow(uint16_t wait) {
	wait/=3;//todo: remove
	// Hue of first pixel runs 5 complete loops through the color wheel.
  // Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to 5*65536. Adding 256 to firstPixelHue each time
  // means we'll make 5*65536/256 = 1280 passes through this outer loop:
  for(long firstPixelHue = 0; firstPixelHue < 65536; firstPixelHue += (256)) {
    for(int i=0; i<this->NumOfPixels(); i++) { // For each pixel in strip...
      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      int pixelHue = firstPixelHue + (i * 65536L / (this->NumOfPixels()));
      // this->ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the single-argument hue variant. The result
      // is passed through this->gamma32() to provide 'truer' colors
      // before assigning to each pixel:
      this->SetPixelColor(i, this->ColorHSV(pixelHue));
    }
    this->Show(); // Update strip with new contents
    osDelay(pdMS_TO_TICKS(wait));  // Pause for a moment
  }
}

// Rainbow-enhanced theater marquee. Pass delay time (in ms) between frames.
void cRGBAnim::TheaterChaseRainbow(int wait) {
  int firstPixelHue = 0;     // First pixel starts at red (hue 0)
  for(int a=0; a<30; a++) {  // Repeat 30 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      this->Clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in increments of 3...
      for(int c=b; c<this->NumOfPixels(); c += 3) {
        // hue of pixel 'c' is offset by an amount to make one full
        // revolution of the color wheel (range 65536) along the length
        // of the strip (this->numPixels() steps):
        int      hue   = firstPixelHue + c * 65536L / this->NumOfPixels();
        uint32_t color = this->ColorHSV(hue); // hue -> RGB
        this->SetPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      this->Show();                // Update strip with new contents
      osDelay(pdMS_TO_TICKS(wait));                 // Pause for a moment
      firstPixelHue += 65536 / 90; // One cycle of color wheel over 90 frames
    }
  }
}



/**
 * \brief     Fill, remove argument
 */
void cRGBAnim::Fill(uint16_t delay)
{
	this->Fill((cWS2812x::sRGB) _RGB_BLUE, (cWS2812x::sRGB) _RGB_GREEN, (cWS2812x::sRGB) _RGB_YELLOW, (cWS2812x::sRGB) _RGB_PURPLE, delay);
}

/**
 * \brief     FillSmooth, remove argument
 */
void cRGBAnim::FillSmooth(uint16_t delay)
{
	extern RNG_HandleTypeDef hrng;
	delay/=8;
	static cWS2812x::sRGB Colors[]={_RGB_BLUE,_RGB_RED, _RGB_ORANGE, _RGB_YELLOW, _RGB_GREEN, _RGB_PURPLE};
	static uint8_t i=0;
	if(this->FillSmooth(Colors[i], delay))
	{
		this->Clear();
		i = HAL_RNG_GetRandomNumber(&hrng) % sizeof(Colors)/sizeof(Colors[0]);
	}
}

/**
 * \brief          Fill diode one by one symmetry and reverse; 1-0-0-0-1,0-1-0-1-0. Remove param
 */
void cRGBAnim::FillSymetryOneV(uint16_t delay)
{
	delay*=4;
	this->FillSymetryOne(delay);
}
