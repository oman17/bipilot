/*
 * rgbAnim.hpp
 *
 *  Created on: 5 lut 2022
 *      Author: MZalas
 */

#ifndef RGBANIMATION_RGBANIM_HPP_
#define RGBANIMATION_RGBANIM_HPP_

#include "ws2812b.hpp"

class cRGBAnim : public cWS2812x{
	public:
	typedef void (cRGBAnim::*VoidFunctionWithOneParameter) (uint16_t delay);
	static const uint8_t _FUNC_CHOOSE = 3;
	static const uint8_t _FUNC_NUMBER = 7;

	typedef enum RGB_STATE_DEF{
		_ONE,
		_TWO,
		_THREE,
		_FOUR,
		_BATT_LOW,
		_DEF
	}RGB_STATE_DEF;

	/* Variable --------------------------------------------------------*/
		cRGBAnim();

		VoidFunctionWithOneParameter FuncBuff[_FUNC_NUMBER]={&cRGBAnim::RainbowSeq, &cRGBAnim::FillRand,
				&cRGBAnim::FillSmooth, &cRGBAnim::FillSymetryOneV,
				&cRGBAnim::Fill, &cRGBAnim::RainbowSeq,
				&cRGBAnim::Rainbow};

		uint8_t FuncBuffIdx[sizeof(FuncBuff)];
		uint8_t ChoosedFunc[_FUNC_CHOOSE];
		uint8_t FunTaken;

		RGB_STATE_DEF State= _DEF;
	/* Functions prototypes --------------------------------------------*/

		void Begin();
		void LoadMany(uint16_t delay);
		void Load();
		void ToggleColor(cWS2812x::sRGB color1 , cWS2812x::sRGB color2 ,uint16_t delay);
		uint8_t Fill(cWS2812x::sRGB color, uint16_t delay);
		uint8_t Fill(cWS2812x::sRGB color1 , cWS2812x::sRGB color2, cWS2812x::sRGB color3 ,uint16_t delay);
		uint8_t Fill(cWS2812x::sRGB color1 , cWS2812x::sRGB color2, cWS2812x::sRGB color3, cWS2812x::sRGB color4 ,uint16_t delay);
		uint8_t FillSmoothAll(uint32_t hue, uint16_t delay);
		uint8_t FillSmooth(cWS2812x::sRGB color ,uint16_t delay);
		uint8_t FillReverse(cWS2812x::sRGB color ,uint16_t delay);
		uint8_t FillReverseDark(uint16_t delay);
		uint8_t FillSymetry(uint16_t delay);
		uint8_t FillSymetryOne(uint16_t delay);
		uint8_t FillSymetryOneFwv(uint16_t delay);
		uint8_t FillSymetryOneRev(uint16_t delay);
		uint8_t ToggleShine(cWS2812x::sRGB color1 , cWS2812x::sRGB color2, cWS2812x::sRGB color3 ,uint16_t delay);
		void FillRand(uint16_t delay);
		uint8_t DarkAllSmoothHSV(uint16_t hue ,uint16_t delay);
		uint8_t DarkAllSmooth(uint16_t delay);
		void RainbowSeq(uint16_t delay);

		void Fill(uint16_t delay);
		void FillSmooth(uint16_t delay);
		void FillSymetryOneV(uint16_t delay);
		//NEOPIXEL
		void TheaterChase(uint32_t color, uint16_t wait);
		void Rainbow(uint16_t wait);
		void TheaterChaseRainbow(int wait);

	private:

		uint8_t ShineDiv = 10;

		void InitChooseRand();
		void InitVar();

};


#endif /* RGBANIMATION_RGBANIM_HPP_ */
