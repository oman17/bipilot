/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"				 
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define _ADD_Pin GPIO_PIN_0
#define _ADD_GPIO_Port GPIOA
#define _RGB_Pin GPIO_PIN_1
#define _RGB_GPIO_Port GPIOA
#define _DFP_TX_Pin GPIO_PIN_2
#define _DFP_TX_GPIO_Port GPIOA
#define _DFP_RX_Pin GPIO_PIN_3
#define _DFP_RX_GPIO_Port GPIOA
#define _ADC_BAT_Pin GPIO_PIN_4
#define _ADC_BAT_GPIO_Port GPIOA
#define _BTN1_Pin GPIO_PIN_5
#define _BTN1_GPIO_Port GPIOA
#define _BTN1_EXTI_IRQn EXTI9_5_IRQn
#define _BTN2_Pin GPIO_PIN_6
#define _BTN2_GPIO_Port GPIOA
#define _BTN2_EXTI_IRQn EXTI9_5_IRQn
#define _BTN3_Pin GPIO_PIN_7
#define _BTN3_GPIO_Port GPIOA
#define _BTN3_EXTI_IRQn EXTI9_5_IRQn
#define _ENC_1_Pin GPIO_PIN_8
#define _ENC_1_GPIO_Port GPIOA
#define _ENC_2_Pin GPIO_PIN_9
#define _ENC_2_GPIO_Port GPIOA
#define _BTN4_Pin GPIO_PIN_10
#define _BTN4_GPIO_Port GPIOA
#define _BTN4_EXTI_IRQn EXTI15_10_IRQn
#define _OLED_CSS_Pin GPIO_PIN_15
#define _OLED_CSS_GPIO_Port GPIOA
#define _OLED_SCK_Pin GPIO_PIN_3
#define _OLED_SCK_GPIO_Port GPIOB
#define _OLED_MISO_Pin GPIO_PIN_4
#define _OLED_MISO_GPIO_Port GPIOB
#define _OLED_MOSI_Pin GPIO_PIN_5
#define _OLED_MOSI_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#define SetBit(var,bit)			(var |= (1<<bit) )
#define ResetBit(var,bit)		(var &= ~(1<<bit) )
#define ReadBitSet(var,bit)		( ((var&(1<<bit)) >=1) ? 1:0)
#define ReadBitReset(var,bit)		( ((var&(1<<bit)) >=1) ? 0:1)

#define _LOW_BATT_V		3.2
#define _MAX_VOLUME		28
//FREERTOS
typedef StaticTask_t osStaticThreadDef_t;
typedef StaticQueue_t osStaticMessageQDef_t;
typedef StaticSemaphore_t osStaticMutexDef_t;
typedef StaticSemaphore_t osStaticSemaphoreDef_t;

/*Enums -----------------------------------------------------------*/

enum TASK_PRIO{
    _tsk_PRIORITY_IDLE=osPriorityIdle,
	_tsk_PRIORITY_LOW=osPriorityLow,
	_tsk_PRIORITY_RGB,
	_tsk_PRIORITY_USB,
    _tsk_PRIORITY_MAX=configMAX_PRIORITIES-1,
};

float Przelicz_zakres(float val, float xa,float ya,float xb,float yb);
void BattCheckRGB();

void SystemClock_Config(void);						  
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
