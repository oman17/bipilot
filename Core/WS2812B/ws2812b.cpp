/********************************************************
* @file				 ws2812b.c
* @author			 Michal Zalas
* @version			 V1.0.0
* @date				 31/03/2020
* @brief			 WS2812B RGB diode function.
********************************************************/

#include <ws2812b.hpp>
#include "rgbAnim.hpp"

extern cRGBAnim RGBAnim;


/**
* @brief Function implementing the RGBHandler thread.
* @param argument: Not used
* @retval None
*/
void cWS2812x::vWS2812RGB(void *argument)
{
  /* USER CODE BEGIN vWS2812RGB */
  /* Infinite loop */
  for(;;)
  {
	  switch(RGBAnim.State)
	  	  {
	  		case(cRGBAnim::RGB_STATE_DEF::_ONE):
	  		{
	  			(RGBAnim.*RGBAnim.FuncBuff[RGBAnim.ChoosedFunc[0]])(100);
	  		  break;
	  		}
	  		case(cRGBAnim::RGB_STATE_DEF::_TWO):
	  		{
	  			(RGBAnim.*RGBAnim.FuncBuff[RGBAnim.ChoosedFunc[1]])(100);
	  			  break;
	  		}
	  		case(cRGBAnim::RGB_STATE_DEF::_THREE):
	  		{

	  			(RGBAnim.*RGBAnim.FuncBuff[RGBAnim.ChoosedFunc[2]])(100);
	  			  break;
	  		}
	  		case(cRGBAnim::RGB_STATE_DEF::_FOUR):
	  		{
	  			(RGBAnim.*RGBAnim.FuncBuff[RGBAnim.ChoosedFunc[1]])(100);
	  			break;
	  		}
	  		case(cRGBAnim::RGB_STATE_DEF::_BATT_LOW):
			{
	  			osDelay(pdMS_TO_TICKS(10));
	  			RGBAnim.SetAll((cWS2812x::sRGB) _RGB_RED);
	  			RGBAnim.Show();
	  			osDelay(pdMS_TO_TICKS(10));
	  			HAL_PWREx_EnterSHUTDOWNMode();
	  			while(1);
	  			break;
			}
	  		default:

	  			RGBAnim.Rainbow(100);
	  			break;
	  	  }
  }
}

cWS2812x::cWS2812x()
{
	extern DMA_HandleTypeDef _WS2812_TIM_DMA;
	extern TIM_HandleTypeDef _WS2812_TIM;

	this->htim = &_WS2812_TIM;
	this->hdma = &_WS2812_TIM_DMA;

	SetBit(this->InitFlag, this->_INIT_MAIN);


}

void cWS2812x::InitRTOS()
{
	this->tRGBHandle = osThreadNew(this->vWS2812RGB, NULL, &this->tRGB_attributes);

	SetBit(this->InitFlag, this->_INIT_RTOS);

	if(ReadBitSet(this->InitFlag, this->_INIT_MAIN) && ReadBitSet(this->InitFlag, this->_INIT_RTOS)
			&& ReadBitReset(this->InitFlag, this->_INIT_ERROR))
	{
		SetBit(this->InitFlag, this->_INIT_OK);
	}
}

/**
 * \brief           Start LEDs update procedure
 * \return          `1` if update done, `0` otherwise
 */
HAL_StatusTypeDef cWS2812x::Show() {
	if(HAL_DMA_GetState(this->hdma)!=HAL_DMA_STATE_READY)
	    return HAL_ERROR;

	this->UpdateSequence();
    return HAL_OK;
}

/**
 * \brief           Clear all LEDS
 * \return          None
 */
void cWS2812x::Clear()
{
	memset(StripColorBuffer,0 ,sizeof(StripColorBuffer));
}

/**
 * \brief           Return number of pixels
 * \return          number of pixels
 */


/**
 * \brief           Update sequence function.
 */
void
cWS2812x::UpdateSequence() {
	HAL_TIM_PWM_Stop_DMA(this->htim, _WS2812_TIM_CH);
	for(uint8_t i=0;i<this->NumPixels ; i++)
	{
		this->UpdatePwmData(i, &RawBuffer[i*24]);
	}
	HAL_TIM_PWM_Start_DMA(this->htim, _WS2812_TIM_CH, (uint32_t*)&RawBuffer[0], sizeof(RawBuffer)/sizeof(_WS2812_TIM_TYPE));
}

/**
 * \brief           Prepares data from memory for PWM output for timer
 * \note            Memory is in format R,G,B, while PWM must be configured in G,R,B[,W]
 * \param[in]       ledx: LED index to set the color
 * \param[out]      ptr: Output array with at least LED_CFG_RAW_BYTES_PER_LED-words of memory
 */
uint8_t
cWS2812x::UpdatePwmData(size_t ledx, _WS2812_TIM_TYPE* ptr) {
    size_t i;

    if (ledx < this->NumPixels) {
        for (i = 0; i < 8; i++) {
            ptr[i] =        (StripColorBuffer[this->_WS2812_BYTES_PER_PIXEL * ledx + 1] & (1 << (7 - i))) ? (2 * _WS2812_TIM_REG->ARR / 3) : (_WS2812_TIM_REG->ARR / 3);
            ptr[8 + i] =    (StripColorBuffer[this->_WS2812_BYTES_PER_PIXEL * ledx + 0] & (1 << (7 - i))) ? (2 * _WS2812_TIM_REG->ARR / 3) : (_WS2812_TIM_REG->ARR / 3);
            ptr[16 + i] =   (StripColorBuffer[this->_WS2812_BYTES_PER_PIXEL * ledx + 2] & (1 << (7 - i))) ? (2 * _WS2812_TIM_REG->ARR / 3) : (_WS2812_TIM_REG->ARR / 3);
#if LED_CFG_USE_RGBW
            ptr[24 + i] =   (StripColorBuffer[this->_WS2812_BYTES_PER_PIXEL * ledx + 3] & (1 << (7 - i))) ? (2 * _WS2812_TIM_REG->ARR / 3) : (_WS2812_TIM_REG->ARR / 3);
#endif /* LED_CFG_USE_RGBW */
        }
        return 1;
    }
    return 0;
}

void cWS2812x::SetBrightness(uint8_t bright)
{
	this->Brightness=bright;
}

uint8_t cWS2812x::NumOfPixels()
{
	return this->NumPixels;
}


/*!
  @brief   Convert separate red, green and blue values into a single
           "packed" 32-bit RGB color.
  @param   r  Red brightness, 0 to 255.
  @param   g  Green brightness, 0 to 255.
  @param   b  Blue brightness, 0 to 255.
  @return  32-bit packed RGB value, which can then be assigned to a
           variable for later use or passed to the setPixelColor()
           function. Packed RGB format is predictable, regardless of
           LED strand color order.
*/
uint32_t cWS2812x::Color(uint8_t r, uint8_t g, uint8_t b)
{
	return ((uint32_t)r << 16) | ((uint32_t)g <<  8) | b;
}

/*!
  @brief   Convert separate red, green, blue and white values into a
           single "packed" 32-bit WRGB color.
  @param   r  Red brightness, 0 to 255.
  @param   g  Green brightness, 0 to 255.
  @param   b  Blue brightness, 0 to 255.
  @param   w  White brightness, 0 to 255.
  @return  32-bit packed WRGB value, which can then be assigned to a
           variable for later use or passed to the setPixelColor()
           function. Packed WRGB format is predictable, regardless of
           LED strand color order.
*/
uint32_t cWS2812x::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
    return ((uint32_t)w << 24) | ((uint32_t)r << 16) | ((uint32_t)g <<  8) | b;
  }

/*!
  @brief   Convert separate red, green, blue values into a
           HSV value.
  @param   r  Red brightness, 0 to 255.
  @param   g  Green brightness, 0 to 255.
  @param   b  Blue brightness, 0 to 255.
  @param   w  White brightness, 0 to 255.
  @return  HSV value,
  	  	  hue, 0 to 360
  	  	  sat, 0 to 255
  	  	  val, 0 to 255
*/
cWS2812x::sHSV cWS2812x::Rgb2Hsv(uint8_t r, uint8_t g, uint8_t b)
{
	sHSV out;
	float min, max , delta;
	float rf,gf,bf;
	rf = r/255.0;
	gf =g/255.0;
	bf = b/255.0;

	min = rf < gf ? rf : gf;
	min = min  < b ? min  : bf;

	max = rf > gf ? rf : gf;
	max = max  > bf ? max  : bf;

	out.val = max * 255.0;
	delta = max - min;

	if (delta < 0.00001)
	{
	  out.sat = 0;
	  out.hue = 0; // undefined, maybe nan?
	  return out;
	}
	if( max > 0.0 ) { // NOTE: if Max is == 0, this divide would cause a crash
	  out.sat = (delta / max) * 255.0;                  // s
	} else {
	  // if max is 0, then rf = gf = bf = 0
	  // s = 0, h is undefined
	  out.sat = 0.0;
	  out.hue = 0.0;                            // its now undefined
	  return out;
	}
	float HueTemp =0 ;
	if( rf >= max )                           // > is bogus, just keeps compilor happy
	  HueTemp = ( gf - bf ) / delta;        // between yellow & magenta
	else
	if( gf >= max )
	  HueTemp = 2.0 + ( bf - rf ) / delta;  // between cyan & yellow
	else
	  HueTemp = 4.0 + ( rf - gf ) / delta;  // between magenta & cyan

	HueTemp *= 60.0;                              // degrees

	if( HueTemp < 0.0 )
	  HueTemp += 360.0;

	out.hue = HueTemp;

	return out;

}

/*!
  @brief   Convert hue, saturation and value into a packed 32-bit RGB color
           that can be passed to setPixelColor() or other RGB-compatible
           functions.
  @param   hue  An unsigned 16-bit value, 0 to 65535, representing one full
                loop of the color wheel, which allows 16-bit hues to "roll
                over" while still doing the expected thing (and allowing
                more precision than the wheel() function that was common to
                prior NeoPixel examples).
  @param   sat  Saturation, 8-bit value, 0 (min or pure grayscale) to 255
                (max or pure hue). Default of 255 if unspecified.
  @param   val  Value (brightness), 8-bit value, 0 (min / black / off) to
                255 (max or full brightness). Default of 255 if unspecified.
  @return  Packed 32-bit RGB with the most significant byte set to 0 -- the
           white element of WRGB pixels is NOT utilized. Result is linearly
           but not perceptually correct, so you may want to pass the result
           through the gamma32() function (or your own gamma-correction
           operation) else colors may appear washed out. This is not done
           automatically by this function because coders may desire a more
           refined gamma-correction function than the simplified
           one-size-fits-all operation of gamma32(). Diffusing the LEDs also
           really seems to help when using low-saturation colors.
*/
uint32_t cWS2812x::ColorHSV(uint16_t hue, uint8_t sat, uint8_t val) {
	 // hue = Calc_range(hue,0,0,360,65535);
  uint8_t r, g, b;

  // Remap 0-65535 to 0-1529. Pure red is CENTERED on the 64K rollover;
  // 0 is not the start of pure red, but the midpoint...a few values above
  // zero and a few below 65536 all yield pure red (similarly, 32768 is the
  // midpoint, not start, of pure cyan). The 8-bit RGB hexcone (256 values
  // each for red, green, blue) really only allows for 1530 distinct hues
  // (not 1536, more on that below), but the full unsigned 16-bit type was
  // chosen for hue so that one's code can easily handle a contiguous color
  // wheel by allowing hue to roll over in either direction.
  hue = (hue * 1530L + 32768) / 65536;
  // Because red is centered on the rollover point (the +32768 above,
  // essentially a fixed-point +0.5), the above actually yields 0 to 1530,
  // where 0 and 1530 would yield the same thing. Rather than apply a
  // costly modulo operator, 1530 is handled as a special case below.

  // So you'd think that the color "hexcone" (the thing that ramps from
  // pure red, to pure yellow, to pure green and so forth back to red,
  // yielding six slices), and with each color component having 256
  // possible values (0-255), might have 1536 possible items (6*256),
  // but in reality there's 1530. This is because the last element in
  // each 256-element slice is equal to the first element of the next
  // slice, and keeping those in there this would create small
  // discontinuities in the color wheel. So the last element of each
  // slice is dropped...we regard only elements 0-254, with item 255
  // being picked up as element 0 of the next slice. Like this:
  // Red to not-quite-pure-yellow is:        255,   0, 0 to 255, 254,   0
  // Pure yellow to not-quite-pure-green is: 255, 255, 0 to   1, 255,   0
  // Pure green to not-quite-pure-cyan is:     0, 255, 0 to   0, 255, 254
  // and so forth. Hence, 1530 distinct hues (0 to 1529), and hence why
  // the constants below are not the multiples of 256 you might expect.

  // Convert hue to R,G,B (nested ifs faster than divide+mod+switch):
  if(hue < 510) {         // Red to Green-1
    b = 0;
    if(hue < 255) {       //   Red to Yellow-1
      r = 255;
      g = hue;            //     g = 0 to 254
    } else {              //   Yellow to Green-1
      r = 510 - hue;      //     r = 255 to 1
      g = 255;
    }
  } else if(hue < 1020) { // Green to Blue-1
    r = 0;
    if(hue <  765) {      //   Green to Cyan-1
      g = 255;
      b = hue - 510;      //     b = 0 to 254
    } else {              //   Cyan to Blue-1
      g = 1020 - hue;     //     g = 255 to 1
      b = 255;
    }
  } else if(hue < 1530) { // Blue to Red-1
    g = 0;
    if(hue < 1275) {      //   Blue to Magenta-1
      r = hue - 1020;     //     r = 0 to 254
      b = 255;
    } else {              //   Magenta to Red-1
      r = 255;
      b = 1530 - hue;     //     b = 255 to 1
    }
  } else {                // Last 0.5 Red (quicker than % operator)
    r = 255;
    g = b = 0;
  }

  // Apply saturation and value to R,G,B, pack into 32-bit result:
  uint32_t v1 =   1 + val; // 1 to 256; allows >>8 instead of /255
  uint16_t s1 =   1 + sat; // 1 to 256; same reason
  uint8_t  s2 = 255 - sat; // 255 to 0
  return ((((((r * s1) >> 8) + s2) * v1) & 0xff00) << 8) |
          (((((g * s1) >> 8) + s2) * v1) & 0xff00)       |
         ( ((((b * s1) >> 8) + s2) * v1)           >> 8);
}

/*
 * R, G, B
 */
uint8_t cWS2812x::GetPixel(uint8_t Number, uint8_t Color)
{
	return StripColorBuffer[Number * this->_WS2812_BYTES_PER_PIXEL + Color];
}

/**
 * \brief           Set R,G,B color for specific LED
 * \param[in]       index: LED index in array, starting from `0`
 * \param[in]       r,g,b: Red, Green, Blue values
 * \return          `1` on success, `0` otherwise
 */
uint8_t
cWS2812x::SetPixelColor(size_t index, uint8_t r, uint8_t g, uint8_t b
#if LED_CFG_USE_RGBW
, uint8_t w
#endif /* LED_CFG_USE_RGBW */
) {
    if (index < this->NumPixels) {
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 0] = r * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 1] =  g * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 2] =  b * (this->Brightness/255.0f);
#if LED_CFG_USE_RGBW
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 3] =  w < this->Brightness ? w : this->Brightness;
#endif /* LED_CFG_USE_RGBW */
        return 1;
    }
    return 0;
}

/**
 * \brief           Set R,G,B color for specific LED
 * \param[in]       index: LED index in array, starting from `0`
 * \param[in]        sRGB
 * \return          `1` on success, `0` otherwise
 */
uint8_t
cWS2812x::SetPixelColor(size_t index, sRGB color
#if LED_CFG_USE_RGBW
, uint8_t w
#endif /* LED_CFG_USE_RGBW */
) {
    if (index < this->NumPixels) {
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 0] = color.r * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 1] =  color.g * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 2] =  color.b * (this->Brightness/255.0f);
#if LED_CFG_USE_RGBW
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 3] =  w * (this->Brightness/255.0f);
#endif /* LED_CFG_USE_RGBW */
        return 1;
    }
    return 0;
}

/**
 * \brief           Set R,G,B color for specific LED
 * \param[in]       index: LED index in array, starting from `0`
 * \param[in]        sRGB
 * \return          `1` on success, `0` otherwise
 */
uint8_t
cWS2812x::SetPixelColorNoBright(size_t index, sRGB color
#if LED_CFG_USE_RGBW
, uint8_t w
#endif /* LED_CFG_USE_RGBW */
) {
    if (index < this->NumPixels) {
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 0] = color.r ;
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 1] =  color.g ;
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 2] =  color.b ;
#if LED_CFG_USE_RGBW
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 3] =  w ;
#endif /* LED_CFG_USE_RGBW */
        return 1;
    }
    return 0;
}

uint8_t
#if LED_CFG_USE_RGBW
cWS2812x::SetPixelColor_rgbw(size_t index, uint32_t rgbw) {
#else /* LED_CFG_USE_RGBW */
cWS2812x::SetPixelColor(size_t index, uint32_t rgbw) {
#endif /* !LED_CFG_USE_RGBW */
    if (index < this->NumPixels) {
//        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 0] = ((rgbw >> 24) & 0xFF) * (this->Brightness/255.0f);
//        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 1] = ((rgbw >> 16) & 0xFF) * (this->Brightness/255.0f);
//        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 2] = ((rgbw >> 8) & 0xFF) * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 0] = ((uint8_t)(rgbw >> 16)) * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 1] = ((uint8_t)(rgbw >> 8)) * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 2] = ((uint8_t)(rgbw)) * (this->Brightness/255.0f);
#if LED_CFG_USE_RGBW
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 3] = ((rgbw >> 0) & 0xFF) * (this->Brightness/255.0f);
#endif /* LED_CFG_USE_RGBW */
        return 1;
    }
    return 0;
}

uint8_t
cWS2812x::SetAll(uint8_t r, uint8_t g, uint8_t b
#if LED_CFG_USE_RGBW
, uint8_t w
#endif /* LED_CFG_USE_RGBW */
) {
    size_t index;
    for (index = 0; index < this->NumPixels; index++) {
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 0] = r * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 1] =  g * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 2] =  b * (this->Brightness/255.0f);
#if LED_CFG_USE_RGBW
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 3] =  w * (this->Brightness/255.0f);
#endif /* LED_CFG_USE_RGBW */
    }
    return 1;
}

uint8_t
cWS2812x::SetAll( sRGB color
#if LED_CFG_USE_RGBW
, uint8_t w
#endif /* LED_CFG_USE_RGBW */
) {
    size_t index;
    for (index = 0; index < this->NumPixels; index++) {
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 0] = color.r * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 1] =  color.g * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 2] =  color.b * (this->Brightness/255.0f);
#if LED_CFG_USE_RGBW
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 3] =  w * (this->Brightness/255.0f);
#endif /* LED_CFG_USE_RGBW */
    }
    return 1;
}

uint8_t
#if LED_CFG_USE_RGBW
cWS2812x::SetPixelColor_all_rgbw(uint32_t rgbw) {
#else /* LED_CFG_USE_RGBW */
cWS2812x::SetAll(uint32_t rgbw) {
#endif /* !LED_CFG_USE_RGBW */
    size_t index;
    for (index = 0; index < this->NumPixels; index++) {
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 0] = ((uint8_t)(rgbw >> 16)) * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 1] = ((uint8_t)(rgbw >> 8)) * (this->Brightness/255.0f);
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 2] = ((uint8_t)(rgbw)) * (this->Brightness/255.0f);
#if LED_CFG_USE_RGBW
        StripColorBuffer[index * this->_WS2812_BYTES_PER_PIXEL + 3] = ((rgbw >> 0) & 0xFF) * (this->Brightness/255.0f);
#endif /* LED_CFG_USE_RGBW */
    }
    return 1;
}

//https://en.wikipedia.org/wiki/Logistic_function
uint8_t cWS2812x::CalcPWM(uint8_t val)
{
    val=val/2.55;
	const float k = 0.1f;
    const float x0 = 60.0f;
    uint8_t rv=300.0f / (1.0f + exp(-k * (val - x0)));
    return rv;
}

//https://en.wikipedia.org/wiki/Logistic_function
cWS2812x::sRGB cWS2812x::CalcPWM(sRGB color)
{
    color.r=color.r/2.55;
    color.g=color.g/2.55;
    color.b=color.b/2.55;
	const float k = 0.1f;
    const float x0 = 60.0f;
    sRGB color_rv;
    color_rv.r=255.0f / (1.0f + exp(-k * (color.r - x0)));
    color_rv.g=255.0f / (1.0f + exp(-k * (color.g - x0)));
    color_rv.b=255.0f / (1.0f + exp(-k * (color.b - x0)));
    return color_rv;
}

/**
 * \brief           Calulate one range to another
 * \param[in]       val: calue to calulcate
 * \param[in]       xa: minimal input of input
 * \param[in]       ya: minimal outpu of input
 * \param[in]       xb: maximal input of input
 * \param[in]       yb: maximal outpu of input
 *
 * \return          new calucated value in y range
 */
float cWS2812x::CalcRange(float val, float xa,float ya,float xb,float yb)
{
  return (ya-yb)/(xa-xb)*val+(ya-(ya-yb)/(xa-xb)*xa);
}


