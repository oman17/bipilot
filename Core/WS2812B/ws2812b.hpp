/********************************************************
* @file				 ws2812b.h
* @author			 Michal Zalas
* @version			 V1.0.0
* @date				 31/03/2020
* @brief			 WS2812B RGB diode function
********************************************************/

#ifndef WS2812B_WS2812B_HPP_
#define WS2812B_WS2812B_HPP_

#include <main.hpp>
#include "stdlib.h"
#include "string.h"
#include "math.h"

#define _WS2812_TIM_REG		TIM2
#define _WS2812_TIM			htim2
#define _WS2812_TIM_CH		TIM_CHANNEL_2
#define _WS2812_TIM_DMA		hdma_tim2_ch2_ch4
#define _WS2812_TIM_TYPE	uint32_t

#define _RGB_WHITE		{0xFF,	0xFF,	0xFF}
#define _RGB_RED		{0xFF,	0x00,	0x00}
#define _RGB_ORANGE		{0xFF,	0xa5,	0x00}
#define _RGB_GREEN		{0x00,	0xFF,	0x00}
#define _RGB_BLUE		{0x00,	0x00,	0xFF}
#define _RGB_PURPLE		{0x80,	0x00,	0x80}
#define _RGB_YELLOW		{0xFF,	0xFF,	0x00}


class cWS2812x {
	public:
	/* Definitions -----------------------------------------------------*/
		static const uint8_t _WS2812_PIXEL_NUM	=	8;	// Number of WS2812B in a strip row

	/* Enums -----------------------------------------------------------*/
		enum INIT_DEF{
			_INIT_ERROR,
			_INIT_OK,
			_INIT_MAIN,
			_INIT_RTOS
		}INIT_DEF;



	/* Structures ------------------------------------------------------*/
		typedef struct {
			uint8_t r;
			uint8_t g;
			uint8_t b;
		}sRGB;

		typedef struct{
			uint16_t hue;
			uint8_t sat;
			uint8_t val;
		}sHSV;

	/* Variable --------------------------------------------------------*/
		uint32_t InitFlag;
		uint8_t Brightness = 0x10;


	/* Functions prototypes --------------------------------------------*/
		cWS2812x();
		void InitRTOS();
		static void vWS2812RGB(void *argument);


		HAL_StatusTypeDef Show();
		void Clear();
		uint8_t NumOfPixels();
		uint8_t GetPixel(uint8_t Number, uint8_t Color);
		uint32_t Color(uint8_t r, uint8_t g, uint8_t b);
		uint32_t Color(uint8_t r, uint8_t g, uint8_t b, uint8_t w);
		sHSV Rgb2Hsv(uint8_t r, uint8_t g, uint8_t b);
		uint32_t ColorHSV(uint16_t hue, uint8_t sat=255, uint8_t val=255);
		uint8_t CalcPWM(uint8_t val);
		sRGB CalcPWM(sRGB color);
		void SetBrightness(uint8_t bright);

		#if _WS2812_USE_RGBW
		uint8_t     SetPixelColor(size_t index, uint8_t r, uint8_t g, uint8_t b, uint8_t w);
		uint8_t     SetPixelColor(size_t index, sRGB color,  uint8_t w);
		uint8_t     SetPixelColor(size_t index, uint32_t rgbw);
		uint8_t 	SetPixelColorNoBright(size_t index, sRGB color,uint8_t w);
		uint8_t     SetAll(uint8_t r, uint8_t g, uint8_t b, uint8_t w);
		uint8_t     SetAll(uint32_t rgbw);
		uint8_t 	SetAll( sRGB color , uint8_t w);
		#else /* LED_CFG_USE_RGBW */
		uint8_t     SetPixelColor(size_t index, uint8_t r, uint8_t g, uint8_t b);
		uint8_t     SetPixelColor(size_t index, uint32_t rgbw);
		uint8_t     SetPixelColor(size_t index, sRGB color);
		uint8_t 	SetPixelColorNoBright(size_t index, sRGB color);
		uint8_t     SetAll(uint8_t r, uint8_t g, uint8_t b);
		uint8_t 	SetAll( sRGB color);
		uint8_t     SetAll(uint32_t rgb);
		#endif /* !LED_CFG_USE_RGBW */

	private:
	/* Definitions -----------------------------------------------------*/

		#if _WS2812_USE_RGBW
		static const uint8_t _WS2812_BYTES_PER_PIXEL	=	4;
		#else /* WS2812_USE_RGBW */
		static const uint8_t _WS2812_BYTES_PER_PIXEL	=	4;
		#endif /* !WS2812_USE_RGBW */

		static const uint16_t _WS2812_RAW_BYTES_PER_PIXEL	= 	(_WS2812_BYTES_PER_PIXEL * 8);	// RGB * 8bit per color

	/* Enums -----------------------------------------------------------*/
	/* Structures ------------------------------------------------------*/
	/* Variable --------------------------------------------------------*/
		DMA_HandleTypeDef* hdma;
		TIM_HandleTypeDef* htim;

		/**
		 * \brief           Array of 4x (or 3x) number of WS2812B (R, G, B[, W] colors)
		 */
		uint8_t StripColorBuffer[_WS2812_BYTES_PER_PIXEL * _WS2812_PIXEL_NUM];
		/**
		 * \brief           Temporary array for dual PIXEL with extracted PWM duty cycles
		 *
		 * We need LED_CFG_RAW_BYTES_PER_LED bytes for PWM setup to send all bits.
		 * Before we can send data for first led, we have to send reset pulse, which must be 50us long.
		 * PWM frequency is 800kHz, to achieve 50us, we need to send 40 pulses with 0 duty cycle = make array size MAX(2 * LED_CFG_RAW_BYTES_PER_LED, 40)
		 */
		_WS2812_TIM_TYPE RawBuffer[_WS2812_PIXEL_NUM * _WS2812_RAW_BYTES_PER_PIXEL + 50];

		//RTOS
		osThreadId_t tRGBHandle;
		uint32_t tvRGBBuffer[ 128 *2 ];
		osStaticThreadDef_t tvRGBControlBlock;
		const osThreadAttr_t tRGB_attributes = {
		  .name = "tRGB",
		  .cb_mem = &tvRGBControlBlock,
		  .cb_size = sizeof(tvRGBControlBlock),
		  .stack_mem = &tvRGBBuffer[0],
		  .stack_size = sizeof(tvRGBBuffer),
		  .priority = static_cast<osPriority_t>(_tsk_PRIORITY_RGB),
		};

		//REST
		uint8_t NumPixels = _WS2812_PIXEL_NUM;

	/* Functions prototypes --------------------------------------------*/
		uint8_t UpdatePwmData(size_t ledx, _WS2812_TIM_TYPE* ptr);
		void UpdateSequence();
		float CalcRange(float val, float xa,float ya,float xb,float yb);
};

#endif /* WS2812B_WS2812B_HPP_ */
